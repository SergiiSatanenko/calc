﻿using Antlr.Runtime;
using System;
using System.Data;

namespace FileCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new int();
            do
            {
                Console.WriteLine($"Программа калькулятор" + "\n" + "1. Для ввода выражения в ручную" + "\n" + "2. Для выбора файла");
                bool wasparsed = int.TryParse(Console.ReadLine(), out menu);                
                if (wasparsed && menu == 1 || menu == 2)                 
                    Console.Clear(); 
                else
                {
                    Console.Clear();
                    Console.WriteLine("Введено не верное значение. Нажмите ввод для того что бы продолжить");
                    Console.ReadLine();
                }
            } while (menu != 1 && menu != 2);

            if (menu == 1)
            {
                Console.Write("Введите выражение: ");
                Console.WriteLine(CalcString.ResultString(Console.ReadLine()));                
            }
            else
            {
                Console.Write("Введите путь к файлу-источнику: ");
                var path1 = Console.ReadLine();
                Console.Write("Введите путь к файлу-назначения: ");
                var path2 = Console.ReadLine();
                var calcFile = new CalcFile(path1);
                calcFile.ResultFile(path2);                
            }
            return;
        }
    }
}