﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace FileCalc
{
    class CalcString
    {
        private static string result;        
        public static string ResultString(string str)
        {
            try
            {
                result = new DataTable().Compute(str, "").ToString();
            }
            catch (Exception)
            {                
                result = "не предвиденный синтаксис";
            }            
            if (result == "∞" || result == "не число")            
                return "Попытка деления на ноль";            
            else
                return result;
        }
    }
    class CalcFile
    {
        private Parser _parcer;
        private string Path;
        private string[] _arStr;
        private string _calc;
        private string[] _result;
        public CalcFile(string path)
        {
            Path = path;
            _arStr = File.ReadAllLines(Path);
        }
        public void ResultFile(string path)
        {
            _result = new String[_arStr.Length];
            for (int i = 0; i < _arStr.Length; i++)
            {
                _parcer = new Parser();
                _calc = _arStr[i];
                
                if (_parcer.Evaluate(_calc))                
                    _result[i] = _arStr[i] + " = " + _parcer.Result;                    
                else
                    _result[i] = _arStr[i] + " - " + "Ошибка выражения";
            }
            File.AppendAllLines(path, _result);
        }
    }
}